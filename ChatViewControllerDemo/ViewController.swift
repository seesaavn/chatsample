//
//  ViewController.swift
//  ChatViewControllerDemo
//
//  Created by Ngoc Luong Nguyen on 9/22/16.
//  Copyright © 2016 Ngoc Luong Nguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    fileprivate let reuseCellIdentifier = "CellId"
    @IBOutlet weak var collectionView: UICollectionView!
    
    let messages = ["How can the society deny the sexual identity to an individual? Why are such individuals shunned by the society? Why stick to the status quo when it comes to sexuality?These questions keep intriguing me", "According to me", "Come to India where sexuality has been deeply woven into the fabric of our mythology."]
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavi()
        collectionView.alwaysBounceVertical = true
        let nibCell = UINib(nibName: "OutgoingBubbleImageViewController", bundle: nil)
        collectionView.register(nibCell, forCellWithReuseIdentifier: reuseCellIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    func configureNavi() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear
    }
}


extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier, for: indexPath) as! OutgoingBubbleImageViewController
        let mess = messages[indexPath.item]
        cell.messageText.text = mess       
        cell.buttonPressed = { string in
            print(string)
        }
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let mess = messages[indexPath.item]
        
        let size = mess.height(withConstrainedWidth: 220, font: UIFont.systemFont(ofSize: 14))
            //.measureTextLabelHeight(maxWidth: 250, font: UIFont.systemFont(ofSize: 14), numberLines: 0)
        return CGSize(width: view.frame.width, height: size + 140)
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("AAAA")
    }
}

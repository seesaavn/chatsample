//
//  StringExtension.swift
//  ChatViewControllerDemo
//
//  Created by Ngoc Luong Nguyen on 7/20/17.
//  Copyright © 2017 Ngoc Luong Nguyen. All rights reserved.
//

import UIKit

public extension String {
    func measureTextLabelHeight(maxWidth: CGFloat, font: UIFont, numberLines: Int) -> CGFloat {
        
        let tempLabel = UILabel(frame: CGRect(x: 0, y: 0, width: maxWidth, height: CGFloat(MAXFLOAT)))
        tempLabel.numberOfLines = numberLines
        tempLabel.text = self
        tempLabel.font = font
        tempLabel.sizeToFit()
        return tempLabel.frame.size.height
        
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
}

//
//  ChatTableViewController.swift
//  ChatViewControllerDemo
//
//  Created by Ngoc Luong Nguyen on 7/24/17.
//  Copyright © 2017 Ngoc Luong Nguyen. All rights reserved.
//

import UIKit

class ChatTableViewController: UIViewController {
    fileprivate let cellId = "Cell"
    @IBOutlet weak var chatTableView: UITableView!
    let messages = ["How can the society deny the sexual identity to an individual? Why are such individuals shunned by the society? Why stick to the status quo when it comes to sexuality?These questions keep intriguing me", "According to me", "Come to India where sexuality has been deeply woven into the fabric of our mythology."]
    var cellHeights: [IndexPath : CGFloat] = [:]
    override func viewDidLoad() {
        
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = false
        let nibCell = UINib(nibName: "CustomTableViewCell", bundle: nil)
        chatTableView.register(nibCell, forCellReuseIdentifier: cellId)
        chatTableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CustomTableViewCell
        cell.messageTextLabel.text = messages[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
}
extension ChatTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return 100 }
        return height
    }
}

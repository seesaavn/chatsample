//
//  OutgoingBubbleImageViewController.swift
//  ChatViewControllerDemo
//
//  Created by Ngoc Luong Nguyen on 9/22/16.
//  Copyright © 2016 Ngoc Luong Nguyen. All rights reserved.
//

import UIKit

class OutgoingBubbleImageViewController: UICollectionViewCell {
    
    @IBOutlet weak var backgroundBubbleView: UIView!
    @IBOutlet weak var messageText: UILabel!
    var buttonPressed: ((String) -> Void)?
    
    @IBOutlet weak var bubbleImageView: UIImageView!
    
       override func awakeFromNib() {
        super.awakeFromNib()
        backgroundBubbleView.layer.cornerRadius = 10
        let imageBubble = UIImage(named: "balloon_blue")?.resizableImage(withCapInsets: UIEdgeInsetsMake(20, 30, 20, 30))
        bubbleImageView.image = imageBubble
    }
    @IBAction func buttonPressed(_ sender: Any) {
        buttonPressed!("Hello")
    }
}

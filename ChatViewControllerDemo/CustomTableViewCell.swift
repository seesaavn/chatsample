//
//  CustomTableViewCell.swift
//  ChatViewControllerDemo
//
//  Created by Ngoc Luong Nguyen on 7/24/17.
//  Copyright © 2017 Ngoc Luong Nguyen. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var messageTextLabel: UILabel!
    @IBOutlet weak var bubbleImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        let imageBubble = UIImage(named: "balloon_blue")?.resizableImage(withCapInsets: UIEdgeInsetsMake(20, 30, 20, 30))
        bubbleImageView.image = imageBubble
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
